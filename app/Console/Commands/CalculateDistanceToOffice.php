<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Proexe\BookingApp\DTO\Point;
use Proexe\BookingApp\Offices\Models\OfficeModel;
use Proexe\BookingApp\Utilities\DistanceCalculator;

class CalculateDistanceToOffice extends Command
{

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bookingApp:calculateDistanceToOffice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Calculates distance to office';
    /**
     * @var DistanceCalculator
     */
    private $distanceCalculator;

    /**
     * Create a new command instance.
     *
     * @param DistanceCalculator $distanceCalculator
     */
    public function __construct(DistanceCalculator $distanceCalculator)
    {
        parent::__construct();

        $this->distanceCalculator = $distanceCalculator;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $offices = OfficeModel::all();

        $lat = $this->ask('Enter Lat:', '14.12232322');
        $lng = $this->ask('Enter Lng:', '8.12232322');

        $from = new Point($lat, $lng);
        $offices->each(function (OfficeModel $office) use ($from) {
            $this->line('Distance to ' . $office->name . ': ' . $this->distanceCalculator->calculate(
                    $from,
                    $office->point,
                    'km'
                )
            );
        });

        $closesOffice = $this->distanceCalculator->findClosestOffice($from);
        if ($closesOffice) {
            $this->line('Closest office: ' . $closesOffice->name);
        }
    }
}
