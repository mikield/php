<?php
/**
 * Date: 30/07/2018
 * Time: 12:48
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */
namespace Proexe\BookingApp\Offices\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Proexe\BookingApp\DTO\Point;

/**
 * @property string name
 * @property float lat
 * @property float lng
 * @property Point point
 *
 * @method static Builder closestTo(float $lat, float $lng)
 */
class OfficeModel extends Model {

	protected $guarded = [];
	protected $table = 'offices';
	protected $casts = [
		'office_hours' => 'array'
	];

	public function getPointAttribute(){
	    return new Point($this->lat, $this->lng);
    }

    public function scopeClosestTo(Builder $query, $latitude, $longitude)
    {
        return $query->selectRaw("
        *,
       ST_Distance_Sphere(
            point(lng, lat),
            point(?, ?)
        ) as distance
    ", [
            $longitude,
            $latitude,
        ])->orderBy('distance');
    }
}
