<?php
/**
 * Date: 08/08/2018
 * Time: 16:20
 * @author Artur Bartczak <artur.bartczak@code4.pl>
 */

namespace Proexe\BookingApp\Utilities;

use Ballen\Distical\Calculator;
use Ballen\Distical\Entities\LatLong;
use Illuminate\Database\DatabaseManager;
use Illuminate\Support\Collection;
use Proexe\BookingApp\DTO\Point;
use Proexe\BookingApp\Offices\Models\OfficeModel;

/**
 * @property Collection distances
 */
class DistanceCalculator
{

    /**
     * @param Point $from
     * @param Point $to
     * @param string $unit - m, km
     *
     * @return mixed
     */
    public function calculate(Point $from, Point $to, $unit = 'm')
    {
        /** @var DatabaseManager $db */
        $db = app('db');

        $distance = $db->select($db->raw($this->getQuery()), [
            'lngFrom' => $from->lng,
            'latFrom' => $from->lat,
            'lngTo' => $to->lng,
            'latTo' => $to->lat,
        ]);

        if (!array_key_exists('0', $distance)) {
            return 0;
        }

        $distance = round(
            $distance[0]->distance,
            2
        );

        $unit = strtoupper($unit);
        if ($unit == "KM") {
            return $distance / 1000;
        }
        return $distance;
    }

    /**
     * @param Point $from
     *
     * @return OfficeModel|null
     */
    public function findClosestOffice(Point $from): ?OfficeModel
    {
        $closestTo = OfficeModel::closestTo($from->lat, $from->lng)->get();
        if ($closestTo->isEmpty()) {
            return null;
        }
        return $closestTo->first();
    }


    public function getQuery()
    {
        return "
            select ST_Distance_Sphere(
                point(:lngFrom, :latFrom),
                point(:lngTo, :latTo)
            ) as distance
        ";
    }

}
